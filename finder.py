from __future__ import annotations

import time
from typing import (
    List,
    Union, Dict
)

from selenium.webdriver import Chrome
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.remote.webdriver import WebDriver
from locators import (
    Locator,
    CATALOG_ITEM,
    CATALOG_ITEM_URL,
    MAIN_SEARCH_BOX,
    MAIN_SEARCH_SUBMIT,
    ITEM_PAGE_CHARACTERISTICS_ITEM,
    ITEM_PAGE_CHARACTERISTICS_ITEM_NAME,
    ITEM_PAGE_PRICE,
    ITEM_PAGE_TITLE,
    ITEM_PAGE_MAIN_IMAGE,
    ITEM_PAGE_RESPONSES_COUNT,
    ITEM_PAGE_CHARACTERISTICS_ITEM_VALUE
)


class Goods:

    """
    Represents a goods in a search query
    """

    def __init__(
            self,
            title: str,
            price: float,
            main_photo: str,
            responses_count: int,
            properties: Dict[str, str],
            root_link: str
    ):
        self.title = title
        self.price = price
        self.main_photo = main_photo
        self.responses_count = responses_count
        self.properties = properties
        self.root_link = root_link

    @staticmethod
    def parse_characteristics(driver: WebDriver) -> Dict[str, str]:
        characteristics = {}

        for element in driver.find_elements(*ITEM_PAGE_CHARACTERISTICS_ITEM):
            name = element.find_element(*ITEM_PAGE_CHARACTERISTICS_ITEM_NAME).text
            characteristics[name] = element.find_element(*ITEM_PAGE_CHARACTERISTICS_ITEM_VALUE).text

        return characteristics

    @staticmethod
    def extract_price(
            driver: WebDriver
    ) -> float:
        price_string = driver.find_element(*ITEM_PAGE_PRICE).text
        return int(price_string.replace(' ', '')[:-1])

    @staticmethod
    def extract_main_photo(
            driver: WebDriver
    ) -> str:
        url_element = driver.find_element(*ITEM_PAGE_MAIN_IMAGE)
        return url_element.get_attribute('src')

    @staticmethod
    def extract_responses_count(
            driver: WebDriver
    ) -> int:
        try:
            return int(driver.find_element(*ITEM_PAGE_RESPONSES_COUNT).text)
        except ValueError:
            return 0

    @staticmethod
    def extract_title(
            driver: WebDriver
    ) -> str:
        return driver.find_element(*ITEM_PAGE_TITLE).text

    @classmethod
    def extract_from_catalog_item(
            cls,
            catalog_item: WebElement,
            driver: WebDriver
    ) -> Goods:
        """

        :param catalog_item: item, which will parsed
        :param driver: webdriver object
        :return:
        """
        previous_window = driver.current_window_handle
        url_element = catalog_item.find_element(*CATALOG_ITEM_URL)
        url = url_element.get_attribute('href')

        # creating new window and switch to it
        driver.switch_to.new_window()
        last_handle = driver.window_handles[-1]
        driver.switch_to.window(last_handle)

        driver.get(url)

        # parsing current item
        item = cls(
            title=cls.extract_title(driver=driver),
            price=cls.extract_price(driver=driver),
            main_photo=cls.extract_main_photo(driver=driver),
            responses_count=cls.extract_responses_count(driver=driver),
            properties=cls.parse_characteristics(driver=driver),
            root_link=driver.current_url,
        )

        # closing tab and switch to previous window
        driver.close()
        driver.switch_to.window(previous_window)

        print(item)

        return item

    def __repr__(self):
        return f'< Item ' \
               f'name=`{self.title}` ' \
               f'price=`{self.price}` ' \
               f'resp_count=`{self.responses_count}` ' \
               f'main_photo_url=`{self.main_photo}` ' \
               f'properties={self.properties} ' \
               f'link=`{self.root_link}` ' \
               f'>'

    @classmethod
    def extract(
            cls,
            extract_from: Union[WebDriver, WebElement],
            driver: WebDriver
    ) -> List[Goods]:
        """
        Extracts item from already searched query
        :param extract_from: extractive element or driver
        :param driver: webdriver object
        :return:
        """
        # searching for catalog items
        catalog_items = extract_from.find_elements(*CATALOG_ITEM)
        items = []

        # and extracting them
        for item in catalog_items:
            parsed_item = cls.extract_from_catalog_item(
                catalog_item=item,
                driver=driver
            )
            items.append(parsed_item)

        return items


class Finder(Chrome):

    def __init__(
            self,
            query: str,
            filters: List[Locator],
            wait_after_action: int = 1,
            **kwargs
    ):
        super(Finder, self).__init__(**kwargs)
        self.query = query
        self.filters = filters
        self.wait_after_action = wait_after_action

    def parse(self) -> List[Goods]:

        self.implicitly_wait(time_to_wait=10)
        self.get('https://rozetka.com.ua/')

        # input search query to search box
        self.find_element(*MAIN_SEARCH_BOX).send_keys(self.query)
        self.find_element(*MAIN_SEARCH_SUBMIT).click()

        # bad waiting after action
        time.sleep(self.wait_after_action)

        # apply filter options
        for filter_ in self.filters:
            self.find_element(*filter_).click()
            time.sleep(self.wait_after_action)

        # and extracts item from search query
        return Goods.extract(
            extract_from=self,
            driver=self
        )
