### Written on python 3.8.2

#### Needs installed Chrome
#### Unpack files and do actions below

```
pip install -r reqs.txt
py run.py
```

If script is not working , change `wait_after_action` argument 
in `run.py` to bigger.
Or try to provide a driver file to `Finder` in `run.py`
