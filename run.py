from finder import Finder
from locators import Filters


if __name__ == '__main__':
    with Finder(
        query='Iphone 14 Pro max',
        filters=[Filters.SELLER_ROZETKA],
        wait_after_action=2
    ) as finder:
        goods = finder.parse()
