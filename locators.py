from typing import Tuple

from selenium.webdriver.common.by import By


Locator = Tuple[str, str]


def extend_locator(
        root_locator: Locator,
        child_locator: Locator
) -> Locator:

    root_type, root_path = root_locator
    child_type, child_path = child_locator

    if child_type != root_type:
        raise ValueError(f'Root locator type `{root_type}` missmatch child type - `{child_type}`')

    return root_type, root_path + child_path


SIDEBAR_BLOCK = (By.CSS_SELECTOR, 'div.sidebar-block')

MAIN_SEARCH_BOX = (By.CSS_SELECTOR, 'input.search-form__input')
MAIN_SEARCH_SUBMIT = (By.CSS_SELECTOR, 'button.search-form__submit')

CATALOG_ITEM = (By.CSS_SELECTOR, 'li.catalog-grid__cell')
CATALOG_ITEM_URL = (By.CSS_SELECTOR, 'a.goods-tile__picture')

ITEM_PAGE_MAIN_IMAGE = (By.CSS_SELECTOR, 'img.picture-container__picture')
ITEM_PAGE_TITLE = (By.CSS_SELECTOR, 'h1.product__title')
ITEM_PAGE_PRICE = (By.CSS_SELECTOR, 'p.product-price__big')
ITEM_PAGE_RESPONSES_COUNT = (By.CSS_SELECTOR, 'h3.product-tabs__heading > span')
ITEM_PAGE_CHARACTERISTICS_ITEM = (By.CSS_SELECTOR, 'div.characteristics-full__item')
ITEM_PAGE_CHARACTERISTICS_ITEM_NAME = (By.CSS_SELECTOR, 'dt')
ITEM_PAGE_CHARACTERISTICS_ITEM_VALUE = (By.CSS_SELECTOR, 'dd')


class Filters:
    SELLER_ROZETKA = extend_locator(
        root_locator=SIDEBAR_BLOCK,
        child_locator=(By.CSS_SELECTOR, '[data-filter-name="seller"] li.checkbox-filter__item:first-of-type')
    )
    SELLER_OTHER = extend_locator(
        root_locator=SIDEBAR_BLOCK,
        child_locator=(By.CSS_SELECTOR, '[data-filter-name="seller"] li.checkbox-filter__item:nth-of-type(2)')
    )